require "application_system_test_case"

class ChartPresetsTest < ApplicationSystemTestCase
  setup do
    @chart_preset = chart_presets(:one)
  end

  test "visiting the index" do
    visit chart_presets_url
    assert_selector "h1", text: "Chart presets"
  end

  test "should create chart preset" do
    visit chart_presets_url
    click_on "New chart preset"

    fill_in "Description content", with: @chart_preset.description_content
    fill_in "Instruction payload", with: @chart_preset.instruction_payload
    fill_in "Title", with: @chart_preset.title
    fill_in "Translation keys", with: @chart_preset.translation_keys
    click_on "Create Chart preset"

    assert_text "Chart preset was successfully created"
    click_on "Back"
  end

  test "should update Chart preset" do
    visit chart_preset_url(@chart_preset)
    click_on "Edit this chart preset", match: :first

    fill_in "Description content", with: @chart_preset.description_content
    fill_in "Instruction payload", with: @chart_preset.instruction_payload
    fill_in "Title", with: @chart_preset.title
    fill_in "Translation keys", with: @chart_preset.translation_keys
    click_on "Update Chart preset"

    assert_text "Chart preset was successfully updated"
    click_on "Back"
  end

  test "should destroy Chart preset" do
    visit chart_preset_url(@chart_preset)
    click_on "Destroy this chart preset", match: :first

    assert_text "Chart preset was successfully destroyed"
  end
end
