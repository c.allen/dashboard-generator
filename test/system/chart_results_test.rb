require "application_system_test_case"

class ChartResultsTest < ApplicationSystemTestCase
  setup do
    @chart_result = chart_results(:one)
  end

  test "visiting the index" do
    visit chart_results_url
    assert_selector "h1", text: "Chart results"
  end

  test "should create chart result" do
    visit chart_results_url
    click_on "New chart result"

    fill_in "Chart preset", with: @chart_result.chart_preset_id
    fill_in "Response link", with: @chart_result.response_link
    fill_in "Status", with: @chart_result.status
    click_on "Create Chart result"

    assert_text "Chart result was successfully created"
    click_on "Back"
  end

  test "should update Chart result" do
    visit chart_result_url(@chart_result)
    click_on "Edit this chart result", match: :first

    fill_in "Chart preset", with: @chart_result.chart_preset_id
    fill_in "Response link", with: @chart_result.response_link
    fill_in "Status", with: @chart_result.status
    click_on "Update Chart result"

    assert_text "Chart result was successfully updated"
    click_on "Back"
  end

  test "should destroy Chart result" do
    visit chart_result_url(@chart_result)
    click_on "Destroy this chart result", match: :first

    assert_text "Chart result was successfully destroyed"
  end
end
