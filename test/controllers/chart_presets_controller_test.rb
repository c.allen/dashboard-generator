require "test_helper"

class ChartPresetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @chart_preset = chart_presets(:one)
  end

  test "should get index" do
    get chart_presets_url
    assert_response :success
  end

  test "should get new" do
    get new_chart_preset_url
    assert_response :success
  end

  test "should create chart_preset" do
    assert_difference("ChartPreset.count") do
      post chart_presets_url, params: { chart_preset: { description_content: @chart_preset.description_content, instruction_payload: @chart_preset.instruction_payload, title: @chart_preset.title, translation_keys: @chart_preset.translation_keys } }
    end

    assert_redirected_to chart_preset_url(ChartPreset.last)
  end

  test "should show chart_preset" do
    get chart_preset_url(@chart_preset)
    assert_response :success
  end

  test "should get edit" do
    get edit_chart_preset_url(@chart_preset)
    assert_response :success
  end

  test "should update chart_preset" do
    patch chart_preset_url(@chart_preset), params: { chart_preset: { description_content: @chart_preset.description_content, instruction_payload: @chart_preset.instruction_payload, title: @chart_preset.title, translation_keys: @chart_preset.translation_keys } }
    assert_redirected_to chart_preset_url(@chart_preset)
  end

  test "should destroy chart_preset" do
    assert_difference("ChartPreset.count", -1) do
      delete chart_preset_url(@chart_preset)
    end

    assert_redirected_to chart_presets_url
  end
end
