require "test_helper"

class ChartResultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @chart_result = chart_results(:one)
  end

  test "should get index" do
    get chart_results_url
    assert_response :success
  end

  test "should get new" do
    get new_chart_result_url
    assert_response :success
  end

  test "should create chart_result" do
    assert_difference("ChartResult.count") do
      post chart_results_url, params: { chart_result: { chart_preset_id: @chart_result.chart_preset_id, response_link: @chart_result.response_link, status: @chart_result.status } }
    end

    assert_redirected_to chart_result_url(ChartResult.last)
  end

  test "should show chart_result" do
    get chart_result_url(@chart_result)
    assert_response :success
  end

  test "should get edit" do
    get edit_chart_result_url(@chart_result)
    assert_response :success
  end

  test "should update chart_result" do
    patch chart_result_url(@chart_result), params: { chart_result: { chart_preset_id: @chart_result.chart_preset_id, response_link: @chart_result.response_link, status: @chart_result.status } }
    assert_redirected_to chart_result_url(@chart_result)
  end

  test "should destroy chart_result" do
    assert_difference("ChartResult.count", -1) do
      delete chart_result_url(@chart_result)
    end

    assert_redirected_to chart_results_url
  end
end
