class ChartResultsController < ApplicationController
  layout 'dashboard'
  before_action :get_chart_preset
  before_action :set_chart_result, only: %i[ show edit update destroy ]

  require 'datadog_api_client'

  # GET /chart_results or /chart_results.json
  def index
    @chart_results = @chart_preset.chart_results
  end

  # GET /chart_results/1 or /chart_results/1.json
  def show
  end

  # GET /chart_results/new
  def new
    @chart_result = @chart_preset.chart_results.build
    @chart_result.input_title = @chart_preset.title
    @chart_result.input_description = @chart_preset.description_content
  end

  # GET /chart_results/1/edit
  def edit
  end

  # POST /chart_results or /chart_results.json
  def create
    @chart_result = @chart_preset.chart_results.build(chart_result_params)
    @chart_result.response_link = generate_datadog_dashboard
    respond_to do |format|
      if @chart_result.save
        format.html { redirect_to chart_preset_chart_result_path(@chart_preset, @chart_result), notice: "Chart result was successfully created." }
        format.json { render :show, status: :created, location: @chart_result }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @chart_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chart_results/1 or /chart_results/1.json
  def update
    respond_to do |format|
      if @chart_result.update(chart_result_params)
        format.html { redirect_to chart_preset_chart_result_path(@chart_result), notice: "Chart result was successfully updated." }
        format.json { render :show, status: :ok, location: @chart_result }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @chart_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chart_results/1 or /chart_results/1.json
  def destroy
    @chart_result.destroy

    respond_to do |format|
      format.html { redirect_to chart_preset_chart_results_path(@chart_preset), notice: "Chart result was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    def get_chart_preset
      @chart_preset = ChartPreset.find(params[:chart_preset_id])
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_chart_result
      @chart_result = @chart_preset.chart_results.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def chart_result_params
      params.require(:chart_result).permit(:response_link, :status, :chart_preset_id, :input_title, :input_description, :branch_name)
    end

    def generate_datadog_dashboard
      api_instance = DatadogAPIClient::V1::DashboardsAPI.new
      # Perform Request
      # response = api_instance.create_dashboard(body)
      body = JSON.parse(@chart_result.chart_preset.instruction_payload)
      # TODO - replace title
      body['title'] = @chart_result.input_title
      body['description'] = @chart_result.input_description
      body['template_variables'].find { |x| x['prefix'] == "@git.branch" }['default'] = @chart_result.branch_name
      response = api_instance.create_dashboard_with_http_info(body)
      return "https://app.datadoghq.com#{response.first.url}"
    end
end
