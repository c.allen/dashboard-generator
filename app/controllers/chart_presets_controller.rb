class ChartPresetsController < ApplicationController
  layout 'dashboard'
  before_action :set_chart_preset, only: %i[ show edit update destroy ]

  # GET /chart_presets or /chart_presets.json
  def index
    @chart_presets = ChartPreset.all
    @chart_results = ChartResult.last(10)
  end

  # GET /chart_presets/1 or /chart_presets/1.json
  def show
  end

  # GET /chart_presets/new
  def new
    @chart_preset = ChartPreset.new
  end

  # GET /chart_presets/1/edit
  def edit
  end

  # POST /chart_presets or /chart_presets.json
  def create
    @chart_preset = ChartPreset.new(chart_preset_params)

    respond_to do |format|
      if @chart_preset.save
        format.html { redirect_to chart_preset_url(@chart_preset), notice: "Chart preset was successfully created." }
        format.json { render :show, status: :created, location: @chart_preset }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @chart_preset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chart_presets/1 or /chart_presets/1.json
  def update
    respond_to do |format|
      if @chart_preset.update(chart_preset_params)
        format.html { redirect_to chart_preset_url(@chart_preset), notice: "Chart preset was successfully updated." }
        format.json { render :show, status: :ok, location: @chart_preset }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @chart_preset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chart_presets/1 or /chart_presets/1.json
  def destroy
    @chart_preset.destroy

    respond_to do |format|
      format.html { redirect_to chart_presets_url, notice: "Chart preset was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chart_preset
      @chart_preset = ChartPreset.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def chart_preset_params
      params.require(:chart_preset).permit(:title, :description_content, :instruction_payload, :translation_keys)
    end
end
