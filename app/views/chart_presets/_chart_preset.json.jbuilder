json.extract! chart_preset, :id, :title, :description_content, :instruction_payload, :translation_keys, :created_at, :updated_at
json.url chart_preset_url(chart_preset, format: :json)
