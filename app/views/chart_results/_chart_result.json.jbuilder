json.extract! chart_result, :id, :response_link, :status, :chart_preset_id, :created_at, :updated_at
json.url chart_result_url(chart_result, format: :json)
