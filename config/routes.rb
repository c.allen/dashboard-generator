Rails.application.routes.draw do
  
  namespace :admin do
      resources :chart_presets

      root to: "chart_presets#index"
    end
    
  resources :chart_presets do
    resources :chart_results
  end
  get 'public/index'
  root 'public#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
