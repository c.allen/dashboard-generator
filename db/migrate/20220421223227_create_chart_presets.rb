class CreateChartPresets < ActiveRecord::Migration[7.0]
  def change
    create_table :chart_presets do |t|
      t.string :title
      t.text :description_content
      t.jsonb :instruction_payload
      t.jsonb :translation_keys

      t.timestamps
    end
  end
end
