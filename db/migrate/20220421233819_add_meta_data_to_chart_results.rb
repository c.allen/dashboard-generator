class AddMetaDataToChartResults < ActiveRecord::Migration[7.0]
  def change
    add_column :chart_results, :input_title, :string
    add_column :chart_results, :input_description, :text
    add_column :chart_results, :branch_name, :string
  end
end
