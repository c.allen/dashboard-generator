class CreateChartResults < ActiveRecord::Migration[7.0]
  def change
    create_table :chart_results do |t|
      t.string :response_link
      t.string :status
      t.integer :chart_preset_id

      t.timestamps
    end
  end
end
