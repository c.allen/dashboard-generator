# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_04_21_233819) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chart_presets", force: :cascade do |t|
    t.string "title"
    t.text "description_content"
    t.jsonb "instruction_payload"
    t.jsonb "translation_keys"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chart_results", force: :cascade do |t|
    t.string "response_link"
    t.string "status"
    t.integer "chart_preset_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "input_title"
    t.text "input_description"
    t.string "branch_name"
  end

end
